export ver=$(jq -r ' .version' package.json)
docker build -t public.ecr.aws/d1k0b9j9/eks-jenkins:latest .
docker tag public.ecr.aws/d1k0b9j9/eks-jenkins:latest public.ecr.aws/d1k0b9j9/eks-jenkins:$ver
docker push public.ecr.aws/d1k0b9j9/eks-jenkins:latest && docker push public.ecr.aws/d1k0b9j9/eks-jenkins:$ver
docker rmi public.ecr.aws/d1k0b9j9/eks-jenkins:latest && docker rmi public.ecr.aws/d1k0b9j9/eks-jenkins:$ver
